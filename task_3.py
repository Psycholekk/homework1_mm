#!/usr/bin/env python3


class Human:
    pass


def print_my_name():
    print("Your name is Milosz")


def add_three_nums(num_1: int, num_2: int, num_3: int):
    print("The result of adding is {}".format((num_1 + num_2 + num_3)))


def hello_world():
    return "Hello world!"


def sub_two_nums(num_1: int, num_2: int):
    return num_1 - num_2


def create_human():
    created_human = Human()
    return created_human


print_my_name()

add_three_nums(3, 3, 3)

returned_string: str = hello_world()
print(returned_string)

returned_int: int = sub_two_nums(4, 3)
print("The result of subtracting is {}".format(returned_int))

returned_human = create_human()
print(returned_human.__class__)
